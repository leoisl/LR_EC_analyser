1/ http://dec2016.archive.ensembl.org/biomart (or the version that you need - see https://www.ensembl.org/info/website/archives/index.html)
2/ Database: Ensembl Genes 87
3/ Dataset: Mouse genes (GRCm38.p5)
4/ Click on Attributes -> Homologues and select these attributes:
    GENE:
        Gene ID
    PARALOGUES:
        Mouse paralogue gene stable ID
        Paralogue %id. target Mouse gene identical to query gene
        Paralogue %id. query gene identical to target Mouse gene
5/ Click on Results and download the file as Compressed file (.gz) format TSV and check Unique results only
    Name it Mus_musculus.GRCm38.87.paralog_genes.txt.gz
6/ gunzip Mus_musculus.GRCm38.87.paralog_genes.txt.gz
7/ Getting the genes with paralogues where the identify is 80%+
    awk '$3>=80 || $4>=80' Mus_musculus.GRCm38.87.paralog_genes.txt > Mus_musculus.GRCm38.87.paralog_genes.80_percent_plus_identity.txt